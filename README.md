# Danger Zone
Air-to-air combat game powered by OpenGL.

## Features
- Arcade flight mechanics
- Realistic terrain
- Enemy variety
- Two weapons: homing missiles and guns
- Particles

## Authors
Michał Miłek & Sebastian Nowak
